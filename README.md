# Introduction
This project contains a header only library making it possible to create, import and export tables within the console easily.

The focus of the project is to make table creation as simple as possible.
# Usage
To use this library just download the `table.hpp` file and add it to your project and include it into any file your wish.
## Precompiler defines
### `FAST_TABLE`
This option enables a faster but less space efficient data structure which is recommended on large data sets with lots of memory overhead.
Disabling this option makes table expansion significantly slower but decreases memory usage so is recommended for projects with fixed table sizes, a small number of resizes (<100), memory contraints.
### `WCHAR_TABLE`
This option enables the use of wide chars and strings.
### '\_WIN32'
This option is required when compiling for Windows.
# Example
Here is an example program to show you how to create this simple table:
```
+----------------+----------------+-----------------+
| first Element  | second Element | third Element   |
|                +----------------+-----------------+
| fourth Element | sixth Element    seventh Element |
+----------------+----------------+-----------------+
| eight Element  | ninth Element  | tenth Element   |
+----------------+----------------+-----------------+
```

```c++
#include "table.h"

int main(void)
{
    types::Table<std::string> table(3,3);

    table.setElement(0,0, "first Element");
    table.setElement(0,1, "second Element");
    table.setElement(0,2, "third Element");
    table.setElement(1,0, "fourth Element");
    table.setElement(1,1, "sixth Element");
    table.setElement(1,2, "seventh Element");
    table.setElement(2,0, "eight Element");
    table.setElement(2,1, "ninth Element");
    table.setElement(2,2, "tenth Element");

    table.setLowerBorderOfCell(0,0, false);
    table.setRightBorderOfCell(1,1, false);

    table.automaticallySizeColumns();
    table.drawTable();
}
```
More examples can be found in the example folder. To compile the examples for Linux just run the provided script `compile_examples.sh`.
