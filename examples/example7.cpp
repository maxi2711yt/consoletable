#define FAST_TABLE
#define WCHAR_TABLE
#include "../table.hpp"
#include <filesystem>
#include <string>

int main(int argc, char** argv)
{
    if(argc != 2) return 1;

    types::Table<string> table(1,1);

    table.importTableFromCSV(std::filesystem::path(argv[1]), DEFAULT_CSV_DIV_CHAR);
    table.automaticallySizeColumns();
    table.drawTable();
}
