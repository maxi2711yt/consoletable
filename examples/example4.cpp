#define WCHAR_TABLE
#define FAST_TABLE
#include "../table.hpp"
#include <string>
#include <filesystem>

typedef struct {
    int a;
    int b;
} custom;

template<> [[nodiscard]] inline string types::to_string(const custom data)
{
    string output = L"(";
    output += types::to_string(data.a);
    output += (wchar_t)'|';
    output += types::to_string(data.b);
    output += (wchar_t)')';
    return output;
}

template<> [[nodiscard]] inline custom types::Table<custom>::to_type(const string value) const noexcept
{
    custom output = {};
    string iter = value.substr(1,value.size() - 2);
    size_t index = iter.find((wchar_t)'|', 0);
    output.a = std::stoi(iter.substr(0, index).c_str());
    output.b = std::stoi(iter.substr(index + 1, iter.size() - index - 1).c_str());
    return output;
}

int main()
{
    types::Table<custom> table(1,1);
    table.importTableFromCSV(std::filesystem::path("example4.csv"), (character)DEFAULT_CSV_DIV_CHAR);
    table.automaticallySizeColumns();
    table.drawTable();
}
