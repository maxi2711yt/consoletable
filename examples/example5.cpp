#define FAST_TABLE
#include "../table.hpp"
#include <iostream>
#include <chrono>

#define SIZE 100000

void genData(types::Table<int>& t)
{
    for(int i = 0; i < SIZE; i++)
    {
        t.addRow();
        t.setElement(t.rows() - 1, 0, rand());
    }
}

template<> [[nodiscard]] inline int types::Table<int>::to_type(const string value) const noexcept
{
    return std::stoi(value);
}

int main(void)
{
    types::Table<int> table(1,1);
   
    auto t1 = std::chrono::high_resolution_clock::now();
    genData(table);
    auto t15 = std::chrono::high_resolution_clock::now();
    table.exportTableAsCSV(std::filesystem::path("output.csv"), DEFAULT_CSV_DIV_CHAR);
    auto t2 = std::chrono::high_resolution_clock::now();
    
    types::Table<int> table2(1,1);
   
    auto t3 = std::chrono::high_resolution_clock::now();
    table2.importTableFromCSV(std::filesystem::path("output.csv"), DEFAULT_CSV_DIV_CHAR);
    auto t4 = std::chrono::high_resolution_clock::now();
    
    std::cout << "Table creation in ms: " << std::chrono::duration_cast<std::chrono::milliseconds>(t15 - t1).count() << std::endl;
    std::cout << "Table export in ms: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t15).count() << std::endl;
    std::cout << "Table import in ms: " << std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count() << std::endl;

    table.removeColumn(0);
    table.removeRow(0);
    table.trim();
    table2.trim();
    table.printTableStats();
    table2.printTableStats();
    return 0;
}
