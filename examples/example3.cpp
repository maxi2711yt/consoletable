#define FAST_TABLE
#include "../table.hpp"


int main(void)
{
    types::Table<std::string> table(3,3);

    table.setColumnHeading(0, "Ingredient");
    table.setElement(0, 0, "Milk");
    table.setElement(1, 0, "Floar");
    table.setElement(2, 0, "Egg");

    table.setColumnHeading(1, "Amount");
    table.setElement(0, 1, "2l");
    table.setElement(1, 1, "500g");
    table.setElement(2, 1, "4");
  
    table.setColumnHeading(2, "Unit Price");
    table.setElement(0, 2, "0.65$");
    table.setElement(1, 2, "0.50$");
    table.setElement(2, 2, "0.10$");

    table.setUpperRightCornerCharOfCell(0, 0, '|');
    table.setUpperRightCornerCharOfCell(0, 1, '|');

    for(size_t i = 0; i < 3; i++)
    {
        table.setAlignmentOfCell(i, 1, types::eAlignment::ALIGN_RIGHT);
        table.setAlignmentOfCell(i, 2, types::eAlignment::ALIGN_RIGHT);
    
        table.setLowerBorderOfCell(0, i, false);
        table.setLowerBorderOfCell(1, i, false);
        table.setLowerBorderOfCell(2, i, false);

        table.setUpperBorderCharOfCell(0, i, ' ');
        table.setLeftBorderOfCell(i, 0, false);
        table.setRightBorderOfCell(i, 2, false);
    }

    table.automaticallySizeColumns();

    table.drawTable();
}
