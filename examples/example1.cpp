#define WCHAR_TABLE
#include "../table.hpp"

[[nodiscard]] inline std::wstring to_string(const std::wstring value)
{
    return value;
}

int main(void)
{
    types::Table<string> table(3,3);

    table.setElement(0,0, L"first Element");
    table.setElement(0,1, L"second Element");
    table.setElement(0,2, L"third Element");
    table.setElement(1,0, L"fourth Element");
    table.setElement(1,1, L"sixth Element");
    table.setElement(1,2, L"seventh Element");
    table.setElement(2,0, L"eight Element");
    table.setElement(2,1, L"ninth Element");
    table.setElement(2,2, L"tenth Element");

    table.setLowerBorderOfCell(0,0, false);
    table.setRightBorderOfCell(1,1, false);
    table.setLowerBorderOfCell(0,2, false);

    table.automaticallySizeColumns();
    table.drawTable();
}
