#define FAST_TABLE
#include "../table.hpp"
#include <string>
#include <filesystem>

const static string data[] = {"Ernst", "Friedrich", "Hans", "Heinrich", "Hermann", "Karl", "Otto", "Paul", "Walter", "Wilhelm", "Anna", "Bertha", "Elisabeth", "Emma", "Frieda ", "Gertrud", "Margarethe", "Maria", "Marie", "Martha", "Gerhard", "Guenter", "Hans", "Heinz", "Helmut", "Herbert", "Karl", "Kurt", "Walter", "Werner", "Edith", "Elfriede", "Erna", "Gerda", "Gertrud", "Hildegard", "Ilse", "Irmgard", "Lieselotte", "Ursula", "Dieter", "Guenter", "Hans", "Horst", "Jürgen", "Klaus", "Manfred", "Peter", "Uwe", "Wolfgang", "Christa", "Elke", "Erika", "Gisela", "Helga", "Ingrid", "Karin", "Monika", "Renate", "Ursula", "Andreas", "Frank", "Jörg", "Jürgen", "Klaus", "Michäl", "Peter", "Stefan", "Thomas", "Uwe", "Andrea", "Angelika", "Birgit", "Gabriele", "Heike", "Martina", "Petra", "Sabine", "Susanne", "Ute", "Alexander", "Christian", "Daniel", "Dennis", "Jan", "Martin", "Michael", "Sebastian", "Stefan", "Thomas", "Anja", "Christina", "Julia", "Katrin", "Melanie", "Nadine", "Nicole", "Sabrina", "Sandra", "Stefanie", "Finn", "Jan", "Jannik", "Jonas", "Leon", "Luca", "Lukas", "Niklas", "Tim", "Tom", "Anna", "Hannah", "Julia", "Lara", "Laura", "Lea", "Lena", "Lisa", "Michelle", "Sarah", "Ben", "Leon", "Paul", "Jonas", "Finn", "Lukas", "Luis", "Luca", "Noah", "Elias", "Mia", "Emma", "Hannah", "Sophia", "Emilia", "Anna", "Lea", "Lina", "Lena", "Marie"};
const static unsigned int count = sizeof(data) / sizeof(string);

int main(void)
{
    types::Table<string> table(20,20);

    for(size_t r = 0; r < table.rows(); r++)
    {
        for(size_t c = 0; c < table.columns(); c++)
        {
            unsigned int index = rand() % count;
            table.setElement(r, c, data[index]);
        }
    }

    table.exportTableAsCSV(std::filesystem::path("testtable.csv"), DEFAULT_CSV_DIV_CHAR);
    return 0;
}
 
