#!/bin/bash

CFLAGS="-pedantic -Wall -Wextra -std=c++17 -ggdb"

echo "example1"
g++ $CFLAGS -g -o out1 example1.cpp
echo "example2"
g++ $CFLAGS -o out2 example2.cpp
echo "example3"
g++ $CFLAGS -o out3 example3.cpp
echo "example4"
g++ $CFLAGS -o out4 example4.cpp
echo "example5"
g++ $CFLAGS -o out5 example5.cpp
echo "example6"
g++ $CFLAGS -o out6 example6.cpp
echo "example7"
g++ $CFLAGS -o out7 example7.cpp
